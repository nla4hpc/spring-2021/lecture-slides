#include <iostream>
#include <mpi.h>


int main(int argc, char *argv[])
{
    int my_rank;
    int size;
    char name[MPI_MAX_PROCESSOR_NAME];
    int len;

    MPI_Init(&argc, &argv);

    // Rank of each process
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    // Name of the node for each rank
    MPI_Get_processor_name(name, &len);

    // Size of the communicator
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    std::cout << "Hello world!, I am process " << my_rank
	      << ", out of " << size << " ranks."
	      << ", running on " << name << " node."
	      << std::endl;

    MPI_Finalize();
    return 0;
}
