# Compile the program
mpicxx hello_world.cpp -o hello_world

#Run the program
mpiexec --report-bindings -n $1 --map-by ppr:$2:node ./hello_world
#mpiexec -n $1 --map-by ppr:$2:node ./hello_world
