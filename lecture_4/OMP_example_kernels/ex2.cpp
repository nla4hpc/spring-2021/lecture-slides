#include <omp.h>
#include <stdio.h>


int main(){

  #pragma omp parallel 
  {
 
    int ID = 0, Z = 0;
    ID = omp_get_thread_num();
    if( ID > 4 ){
        printf(�ID: %d\n�, ID);
    } else {
        #pragma omp atomic
        {
          a = a + ID;
        }
        #pragma omp barrier
        printf(�a: %d\n�, a);
    }

  }
  return 0;
}